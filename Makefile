
NO_COLOR=\033[0m
OK_COLOR=\033[32;01m
GOLANGCI_CMD := $(shell command -v golangci-lint 2> /dev/null)
PKGS := $(shell go list ./... | grep -v /vendor/ | grep -v /internal/mock)
ALL_PACKAGES := $(shell go list ./... | grep -v /vendor/ | grep -v /internal/mock)
PG_MIGRATIONS_FOLDER=./scripts/pgbo/migrations
PG_DB_URL=postgresql://postgres:postgres@localhost:5439/db_mig_test?sslmode=disable

CMD_SQLC := $(shell command -v sqlc 2> /dev/null)
CMD_MIGRATE := $(shell command -v migrate 2> /dev/null)

check-migrations-cmd:
ifndef CMD_MIGRATE
	$(error "migrate is not installed, see: https://github.com/golang-migrate/migrate")
endif
ifndef CMD_SQLC
	$(error "sqlc is not installed, see: github.com/kyleconroy/sqlc)")
endif

check-golangci:
ifndef GOLANGCI_CMD
	$(error "Please install golangci linters from https://golangci-lint.run/usage/install/")
endif

lint: check-golangci fmt
	@echo -e "$(OK_COLOR)==> linting projects$(NO_COLOR)..."
	@golangci-lint run --fix

fmt:
	@go fmt $(ALL_PACKAGES)

deps:
	go mod tidy && go mod vendor

gen.pg.models: check-migrations-cmd
	cd ./scripts/pgbo && sqlc generate

gen.pg.migration: check-migrations-cmd
	migrate create -ext sql -dir $(PG_MIGRATIONS_FOLDER) --seq $(name)

pg.migrate.up: check-migrations-cmd
	migrate -path $(PG_MIGRATIONS_FOLDER) -database $(PG_DB_URL) --verbose up

pg.migrate.down: check-migrations-cmd
	migrate -path $(PG_MIGRATIONS_FOLDER) -database $(PG_DB_URL) --verbose down

run-service-local:
	go run cmd/api/application.go
