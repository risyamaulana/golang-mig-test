## IDE Setup

```console
cd ~
go get mvdan.cc/gofumpt
which gofumpt
```

### VSCode

* gofumpt. see [vscode gfumpt setup](https://github.com/mvdan/gofumpt#visual-studio-code)

* golangci-lint

    ```json
    "go.lintTool":"golangci-lint",
    "go.lintFlags": [
    "--fast"
    ]
    ```

### GoLand

* gofumpt. see [goland gofumpt setup](https://github.com/mvdan/gofumpt#goland)

* golangci-lint

    1. Install `File Watchers` plugin from Intellij Plugin Marketplace.

    2. Settings -> Tools -> File Watchers -> Click + -> `golangci-lint`

    3. Arguments : `run --fix --fast $FileDir$`s

## Local development

* Copy file `config.yaml.example` to `config.yaml`
* run with `make run-service-local`

