package echohttp

import (
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
)

func handleEchoError(_ config.KVStore) echo.HTTPErrorHandler {
	return func(err error, ctx echo.Context) {
		var echoError *echo.HTTPError

		// if *echo.HTTPError, let echokit middleware handles it
		if errors.As(err, &echoError) {
			return
		}

		statusCode := http.StatusInternalServerError
		message := err.Error()

		switch {
		case errors.Is(err, httpservice.ErrBadRequest) || errors.Is(err, httpservice.ErrMissingHeaderTokenData) || errors.Is(err, httpservice.ErrMissingHeaderRefreshTokenData) || errors.Is(err, httpservice.ErrConfirmPasswordNotMatch):
			statusCode = http.StatusBadRequest
			message = err.Error()
		case errors.Is(err, httpservice.ErrInvalidAppKey) || errors.Is(err, httpservice.ErrInvalidToken) || errors.Is(err, httpservice.ErrDataUserUnauthorized) || errors.Is(err, httpservice.ErrDataUserInactive) || errors.Is(err, httpservice.ErrDataUserNotFound) || errors.Is(err, httpservice.ErrUserCheckin) || errors.Is(err, httpservice.ErrPasswordNotMatch):
			statusCode = http.StatusUnauthorized
			message = err.Error()
		case errors.Is(err, httpservice.ErrDataNotFound):
			statusCode = http.StatusNotFound
			message = err.Error()
		case errors.Is(err, httpservice.ErrUserAlreadyCheckin) || errors.Is(err, httpservice.ErrUserAlreadyCheckout):
			statusCode = http.StatusUnprocessableEntity
			message = err.Error()
		case errors.Is(err, httpservice.ErrUnknownSource) || errors.Is(err, httpservice.ErrInternalService):
			statusCode = http.StatusInternalServerError
			message = err.Error()
		}

		_ = ctx.JSON(statusCode, echo.NewHTTPError(statusCode, message))
	}
}
