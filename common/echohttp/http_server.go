package echohttp

import (
	"context"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/echokit"
	"net/http"

	authTokenApp "gitlab.com/risyamaulana/golang-mig-test/src/auth_token/application"

	authorizationUserApp "gitlab.com/risyamaulana/golang-mig-test/src/authorization/user/application"

	userApp "gitlab.com/risyamaulana/golang-mig-test/src/user/application"
	userActivityApp "gitlab.com/risyamaulana/golang-mig-test/src/user_activity/application"
	userAttendanceApp "gitlab.com/risyamaulana/golang-mig-test/src/user_attendance/application"
)

func RunEchoHTTPService(ctx context.Context, s *httpservice.Service, cfg config.KVStore) {
	e := echo.New()
	e.HTTPErrorHandler = handleEchoError(cfg)

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowCredentials: true,
		AllowMethods:     []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete, http.MethodOptions},
		AllowHeaders:     []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, cfg.GetString("header.token-param"), cfg.GetString("header.refresh-token-param")},
	}))

	runtimeCfg := echokit.NewRuntimeConfig(cfg, "restapi")
	runtimeCfg.HealthCheckFunc = s.GetServiceHealth

	e.Use(middleware.Logger())
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	})

	// Auth token route
	authTokenApp.AddRouteAuthToken(s, cfg, e)

	// Authorization User
	authorizationUserApp.AddRouteAuthorizationUser(s, cfg, e)

	// User
	userApp.AddRouteUser(s, cfg, e)
	userAttendanceApp.AddRouteUserAttendance(s, cfg, e)
	userActivityApp.AddRouteUserActivity(s, cfg, e)

	// run actual server
	echokit.RunServerWithContext(ctx, e, runtimeCfg)
}
