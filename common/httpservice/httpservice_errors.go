package httpservice

import "errors"

// error message.
var (
	ErrBadRequest                    = errors.New("bad request payload")
	ErrMissingHeaderTokenData        = errors.New("missing header `token` data")
	ErrMissingHeaderRefreshTokenData = errors.New("missing header `refresh-token` data")

	ErrInvalidAppKey = errors.New("invalid request app key")
	ErrInvalidToken  = errors.New("invalid request token")

	ErrDataUserUnauthorized = errors.New("unauthorized user")
	ErrDataUserNotFound     = errors.New("user not found")
	ErrDataUserInactive     = errors.New("user is not active")

	ErrUserCheckin         = errors.New("please check-in first")
	ErrUserAlreadyCheckin  = errors.New("user already check-in")
	ErrUserAlreadyCheckout = errors.New("user already check-out")

	ErrDataNotFound = errors.New("data not found")

	ErrPasswordNotMatch        = errors.New("password not match")
	ErrConfirmPasswordNotMatch = errors.New("confirm password not match")

	ErrInternalService = errors.New("internal service error")
	ErrUnknownSource   = errors.New("unknown error")
)

// error message.
var (
	MsgHeaderTokenNotFound     = "Header `token` not found"
	MsgHeaderTokenUnauthorized = "Unauthorized token data"

	MsgHeaderRefreshTokenNotFound     = "Header `refresh-token` not found"
	MsgHeaderRefreshTokenUnauthorized = "Unauthorized refresh-token data"

	MsgIsNotLogin    = "Please login first"
	MsgUserNotFound  = "User not found"
	MsgUserNotActive = "User not active"
)
