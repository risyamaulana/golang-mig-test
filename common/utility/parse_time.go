package utility

import (
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"time"
)

func ParseTimeToDate(cfg config.KVStore, timestamp time.Time) (date string) {
	date = timestamp.Format(cfg.GetString("time.default-date-format"))
	return
}

func ParseTimestampToTime(cfg config.KVStore, timestamp time.Time) (time string) {
	time = timestamp.Format(cfg.GetString("time.default-time-format"))
	return
}
