# Introduction

## Background

## Priority

Dalam pembuatan program ini, memprioritaskan hal-hal berikut: 

1. *Correctness*, program harus berjalan sesuai dengan tujuan & fungsi
   bisnisnya. Percuma menggunakan teknologi terbaru, library populer tapi
   program tidak berjalan sesuai ekspektasi fungsi & requirement. Meskipun
   demikian, bugs logic & error pada program pasti akan  dijumpai.
2. *Readability*, program mudah dibaca oleh anggota tim lainnya. Oleh
   karena itu, perlu standar yg sama dalam hal penamaan route path, variable,
   file, struktur & organisasi foldernya. Termasuk logging, karena terkait
   readability dalam proses debugging.
4. *Performace*, jika program sudah berjalan dengan benar & source code-nya
   rapih, baru kita pikirkan performance optimization. Dengan menggunakan
   GoLang, 90% kita sudah terbantu dalam hal performa & utilisasi resource
   server.

## Teknologi

Berikut adalah teknologi yang diimplementasikan dalam pengembangan program ini:

* HTTP JSON API dari mobile app menggunakan `echo.Echo`
* RDBMS menggunakan postgres
* Migration menggunakan SQLC
