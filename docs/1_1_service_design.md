# GoLang Service Design

## Data Flow

1. Request user masuk ke REST API endpoint
2. Validasi input user di level handler/controller. Validasi tidak meliputi
   pengecekan ke external system. Murni hanya pengecekan
   berdasarkan payload request-nya.
3. Build request untuk service object, lalu call fungsi service.
4. Berdasarkan response dari service object, handler/controller menentukan:
    * Jika sukses, build & return ok response / payload ke user.
    * Jika gagal, return error-nya berdasarkan error handling.

## Service method naming guide

Service method [Create, Get, Put, Delete, Update, List] prefix.
e.g. :

* `Create` untuk membuat resource baru (baik menulis ke storage / tidak)
    * contoh: `CreateUser(ctx context.Context, ID string) (User, error)`
* `Get` untuk mendapatkan _*single*_ resource yang sudah ada sebelumnya di
    datasource
    * contoh: `GetUser(ctx context.Context, ID string) (User, error)`
* `Update` untuk mengupdate *single* resource yang sudah ada sebelumnya secara
    keseluruhan (replace existing resource)
    * contoh: `UpdateUser(ctx context.Context, u *User)error`
* `Put` untuk mengupdate *single* resource secara parsial.
* `List` untuk mendapatkan *many* resources yang sudah ada sebelumnya di
    datasrouce
    * `ListUser(ctx context.Context, options FilterOptions) ([]User, error)`
* `Delete` untuk menghapus *single* resource di datasource
    * `DeleteUser(ctx context.Context, ID string) error`

## Reference

* [CRUD-y By Design](https://github.com/adamwathan/laracon2017)
