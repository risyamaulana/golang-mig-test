# Echo HTTP API Design

Banyak cara untuk implement http json web server di go. Bisa menggunakan std
lib `net/http`, maupun go web-framework (iris, fiber, gin, gorrilla atau echo).

Di sini akan dijelaskan terkait standar penggunakan echo webframework.

## Tugas dari http JSON layer / controller / handler

* Expose fungsi dari `src/service`. 1 fungsi endpoint handler dapat
  dimapping ke 1 service method.
* Melakukan validasi input dari user. Validasi yang dilakukan tidak cek
    level external db / storage. Jadi cukup cek format payload. Untuk validasi
    yg cek ke external API, db atau storage lainnya, bisa ditempatkan di level
    http middleware & `src/service` sendiri.
* Build parameter untuk service method yang akan dipanggil.
* Call service method yg diexpose
* Build response balikan ke user sesuai dengan hasil service method tsb.
* Handle error yang di-return (jika ada).
