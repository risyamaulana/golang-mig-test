# Data Access / Repository

## Storage Redis / MongoDB (manual repository impl)



## Storage SQL (Postgres)

Karena menggunakan Postgres sebagai salah satu RDBMS, maka akses
data-nya cukup menggunakan [golang library sqlc](https://docs.sqlc.dev/en/latest/).
Tidak perlu di wrap dalam `repository` object. Cukup simpan instance `*sql.DB` di
dalam object `service`, dan create query object di setiap fungsi yg
membutuhkan.

```golang
// internal/service/service.go
type Service struct {
    db *sql.DB      // digunakan untuk akses data ke postgres
    ...
}

// internal/service/user_service.go
func (s *Service) GetUser(ctx context.Context, ID int) (*User, error) {
    // create query obj
    query := sqlc.New(s.db)
    u, err := query.FindUserByID(ctx, ID)

    ...
}
```