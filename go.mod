module gitlab.com/risyamaulana/golang-mig-test

go 1.16

require (
	github.com/HereMobilityDevelopers/mediary v1.0.0
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/go-playground/validator/v10 v10.10.1
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/iancoleman/strcase v0.2.0
	github.com/labstack/echo-contrib v0.12.0
	github.com/labstack/echo/v4 v4.7.2
	github.com/lithammer/shortuuid/v3 v3.0.7
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.26.1
	github.com/spf13/cast v1.4.1
	github.com/spf13/viper v1.11.0
	go.elastic.co/apm/module/apmsql v1.15.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
