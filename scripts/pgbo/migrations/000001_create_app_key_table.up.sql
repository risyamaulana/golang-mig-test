CREATE SEQUENCE IF NOT EXISTS mig_app_keys_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE IF NOT EXISTS mig_app_keys
(
    id bigint NOT NULL DEFAULT nextval('mig_app_keys_id_seq'::regclass),
    name character varying(100) NOT NULL,
    key character varying(200) NOT NULL,
    CONSTRAINT app_key_pkey PRIMARY KEY (id),
    CONSTRAINT uq_app_key_name UNIQUE (name),
    CONSTRAINT uq_app_key_key UNIQUE (key)
    );

-- default insert value
INSERT INTO mig_app_keys(name, key)
VALUES ('mig-dev', 'm19-d3V') ON CONFLICT DO NOTHING;