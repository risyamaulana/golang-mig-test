CREATE SEQUENCE IF NOT EXISTS mig_users_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;


CREATE TABLE IF NOT EXISTS mig_users
(
    id bigint NOT NULL DEFAULT nextval('mig_users_id_seq'::regclass),
    guid character varying(40) NOT NULL,
    name character varying(150) NOT NULL,
    email character varying NOT NULL,
    gender character varying(10) NOT NULL,
    address text,
    salt character varying(50) NOT NULL,
    password character varying NOT NULL,
    is_active boolean NOT NULL DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(40) NOT NULL,
    updated_at timestamp without time zone,
    last_login timestamp without time zone,
    CONSTRAINT user_pkey PRIMARY KEY (guid),
    CONSTRAINT uq_email_user UNIQUE (email)
);

INSERT INTO "public"."mig_users"("guid", "name", "email", "gender", "address", "salt", "password", "is_active", "created_at", "created_by")
VALUES ('498ed622-e34e-493a-9506-839c1268123a', 'Risya Maulana', 'risyamaulana25@gmail.com', 'Male', 'Jl. Guntur Sari II No.33 Turangga, Lengkong, Bandung', 'JtnmW', 'b268ed885c92bead28dc6118b8b7656f491b0d0f', 't', (now() at time zone 'UTC')::TIMESTAMP, 'system');