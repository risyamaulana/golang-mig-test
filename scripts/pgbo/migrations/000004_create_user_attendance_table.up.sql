CREATE SEQUENCE IF NOT EXISTS mig_users_attendance_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;


CREATE TABLE IF NOT EXISTS mig_users_attendance
(
    id bigint NOT NULL DEFAULT nextval('mig_users_attendance_id_seq'::regclass),
    user_guid character varying(40) NOT NULL,
    attendance_date timestamp without time zone NOT NULL,
    check_in timestamp without time zone NOT NULL,
    check_out timestamp without time zone,
    PRIMARY KEY (id),
    CONSTRAINT fk_user_guid FOREIGN KEY (user_guid)
    REFERENCES public.mig_users (guid)
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
