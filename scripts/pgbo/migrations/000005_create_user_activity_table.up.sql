CREATE SEQUENCE IF NOT EXISTS mig_user_activities_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;


CREATE TABLE IF NOT EXISTS mig_user_activities
(
    id bigint NOT NULL DEFAULT nextval('mig_user_activities_id_seq'::regclass),
    attendance_id bigint NOT NULL,
    activity text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    PRIMARY KEY (id),
    CONSTRAINT fk_user_attendance FOREIGN KEY (attendance_id)
    REFERENCES public.mig_users_attendance (id)
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
