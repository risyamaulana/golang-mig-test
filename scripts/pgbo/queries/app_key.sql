-- name: GetAppKeyByName :one
SELECT
    ak.id,
    ak.name,
    ak.key
FROM mig_app_keys ak
WHERE
        ak.name = @name;