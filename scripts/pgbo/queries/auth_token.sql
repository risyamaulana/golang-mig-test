-- name: InsertAuthToken :one
INSERT INTO mig_auth_tokens
    (name, device_id, device_type, token, token_expired, refresh_token, refresh_token_expired, is_login, user_login, created_at)
VALUES
    (@name, @device_id, @device_type, @token, @token_expired, @refresh_token, @refresh_token_expired, @is_login, @user_login, (now() at time zone 'UTC')::TIMESTAMP)
    ON CONFLICT (name, device_id, device_type) DO
UPDATE
    SET token = @token,
    token_expired = @token_expired,
    refresh_token = @refresh_token,
    refresh_token_expired = @refresh_token_expired,
    is_login = @is_login,
    user_login = @user_login,
    updated_at = (now() at time zone 'UTC')::TIMESTAMP
    RETURNING mig_auth_tokens.*;

-- name: RecordAuthTokenUserLogin :exec
UPDATE mig_auth_tokens
SET
    user_login = @user_login,
    is_login = true,
    updated_at = (now() at time zone 'UTC')::TIMESTAMP
WHERE
    name = @name
    AND device_id = @device_id
    AND device_type = @device_type;

-- name: ClearAuthTokenUserLogin :exec
UPDATE mig_auth_tokens
SET
    is_login = false,
    user_login = null,
    updated_at = (now() at time zone 'UTC')::TIMESTAMP
WHERE
    name = @name
    AND device_id = @device_id
    AND device_type = @device_type;

-- name: GetAuthToken :one
SELECT mat.*
FROM mig_auth_tokens mat
WHERE
    mat.name = @name
    AND mat.device_id = @device_id
    AND mat.device_type = @device_type;