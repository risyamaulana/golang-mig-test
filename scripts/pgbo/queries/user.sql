-- name: RecordUserLastLogin :exec
UPDATE mig_users
SET
    last_login = (now() at time zone 'UTC')::TIMESTAMP
WHERE
    guid = @guid;


-- name: UpdateUser :one
UPDATE mig_users
SET
    name = @name,
    gender = @gender,
    address = @address,
    updated_at = (now() at time zone 'UTC')::TIMESTAMP
WHERE
    guid = @guid
RETURNING mig_users.*;

-- name: UpdateUserPassword :exec
UPDATE mig_users
SET
    salt = @salt,
    password = @password,
    updated_at = (now() at time zone 'UTC')::TIMESTAMP
WHERE
    guid = @guid;

-- name: GetUser :one
SELECT
    mu.*
FROM mig_users mu
WHERE
    mu.guid = @guid;


-- name: GetUserByEmail :one
SELECT
    mu.*
FROM mig_users mu
WHERE
    mu.email = @email;