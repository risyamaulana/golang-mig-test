-- name: InsertUserActivity :one
INSERT INTO mig_user_activities(
    attendance_id, activity, created_at)
VALUES (@attendance_id, @activity, (now() at time zone 'UTC')::TIMESTAMP)
RETURNING mig_user_activities.*;


-- name: UpdateUserActivity :one
UPDATE mig_user_activities
SET
    activity = @activity,
    updated_at =(now() at time zone 'UTC')::TIMESTAMP
WHERE
    id = @id
RETURNING mig_user_activities.*;

-- name: DeleteUserActivity :exec
UPDATE mig_user_activities
SET
    deleted_at =(now() at time zone 'UTC')::TIMESTAMP
WHERE
    id = @id;

-- name: ListUserActivity :many
SELECT
    mua."id" AS attendance_id,
    mua.attendance_date,
    mua.check_in,
    mua.check_out,
    muact.id AS activity_id,
    muact.activity,
    muact.created_at,
    muact.updated_at
FROM
    mig_user_activities muact
    LEFT JOIN mig_users_attendance mua ON mua."id" = muact.attendance_id
WHERE
    mua.attendance_date BETWEEN @attendance_date_from AND @attendance_date_to
    AND mua.user_guid = @user_guid
    AND muact.deleted_at IS NULL
ORDER BY mua.attendance_date DESC, muact.id ASC;


-- name: GetUserActivitiesByAttendance :many
SELECT
    *
FROM mig_user_activities mua
WHERE
    mua.attendance_id = @attendance_id
    AND mua.deleted_at IS NULL
ORDER BY mua.id ASC;
