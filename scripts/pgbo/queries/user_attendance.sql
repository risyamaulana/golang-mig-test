-- name: RecordUserCheckin :exec
INSERT INTO mig_users_attendance
(user_guid, attendance_date, check_in)
VALUES
    (@user_guid, (now() at time zone 'UTC')::TIMESTAMP, (now() at time zone 'UTC')::TIMESTAMP);

-- name: RecordUserCheckout :exec
UPDATE mig_users_attendance
SET
    check_out =  (now() at time zone 'UTC')::TIMESTAMP
WHERE
    id = @id;

-- name: ListUserAttendance :many
SELECT mua.*
FROM mig_users_attendance mua
WHERE
    mua.attendance_date BETWEEN @attendance_date_from AND @attendance_date_to
    AND mua.user_guid = @user_guid
ORDER BY mua.attendance_date DESC;


-- name: GetUserAttendanceByUserDate :one
SELECT mua.*
FROM mig_users_attendance mua
WHERE
    mua.user_guid = @user_guid
    AND TO_CHAR(mua.attendance_date, 'YYYY-MM-DD') = @attendance_date::character varying;