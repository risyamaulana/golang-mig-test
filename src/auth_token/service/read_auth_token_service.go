package service

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
)

func (s *AuthTokenService) ReadAuthToken(ctx context.Context, request sqlc.GetAuthTokenParams) (authToken sqlc.MigAuthToken, err error) {
	q := sqlc.New(s.mainDB)

	authToken, err = q.GetAuthToken(ctx, request)
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed get auth token")
		err = errors.WithStack(httpservice.ErrInvalidToken)

		return
	}

	return
}
