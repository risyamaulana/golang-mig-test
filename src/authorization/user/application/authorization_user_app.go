package application

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/common/jwt"
	"gitlab.com/risyamaulana/golang-mig-test/src/authorization/user/service"
	"gitlab.com/risyamaulana/golang-mig-test/src/middleware"
	"gitlab.com/risyamaulana/golang-mig-test/src/repository/payload"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
	"net/http"
)

func AddRouteAuthorizationUser(s *httpservice.Service, cfg config.KVStore, e *echo.Echo) {
	svc := service.NewAuthorizationUserService(s.GetDB(), cfg)

	mddw := middleware.NewEnsureToken(s.GetDB(), cfg)
	authorizationUser := e.Group("/authorization/user")
	authorizationUser.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "authorization user ok")
	})
	authorizationUser.Use(mddw.ValidateToken)

	authorizationUser.POST("/login", loginUser(svc))
	authorizationUser.POST("/logout", logoutUser(svc))
}

func loginUser(svc *service.AuthorizationUserService) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		var request payload.AuthorizationUserPayload

		if err := ctx.Bind(&request); err != nil {
			log.FromCtx(ctx.Request().Context()).Error(err, "failed to parse request")
			return errors.WithStack(httpservice.ErrBadRequest)
		}

		// Validate request
		if err := request.Validate(); err != nil {
			return err
		}

		data, err := svc.Login(ctx.Request().Context(), request, ctx.Get("token-data").(jwt.RequestJWTToken))
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, payload.ToPayloadUser(data), nil)
	}
}

func logoutUser(svc *service.AuthorizationUserService) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		err := svc.Logout(ctx.Request().Context(), ctx.Get("token-data").(jwt.RequestJWTToken))
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, nil, nil)
	}
}
