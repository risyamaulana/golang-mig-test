package service

import (
	"context"
	"database/sql"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/common/jwt"
	"gitlab.com/risyamaulana/golang-mig-test/common/utility"
	"gitlab.com/risyamaulana/golang-mig-test/src/repository/payload"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"

	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	userService "gitlab.com/risyamaulana/golang-mig-test/src/user/service"
)

func (s *AuthorizationUserService) Login(ctx context.Context, request payload.AuthorizationUserPayload, jwtRequest jwt.RequestJWTToken) (user sqlc.MigUser, err error) {
	userSvc := userService.NewUserService(s.mainDB, s.cfg)

	// Check user by mail
	user, err = userSvc.GetUserByEmail(ctx, request.Email)
	if err != nil {
		return
	}

	// Check user password valid
	password := utility.GeneratePassword(user.Salt, request.Password)
	if password != user.Password {
		err = errors.WithStack(httpservice.ErrPasswordNotMatch)
		return
	}

	// check active user {
	if !user.IsActive {
		err = errors.WithStack(httpservice.ErrDataUserInactive)
		return
	}

	tx, err := s.mainDB.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed begin tx")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	q := sqlc.New(s.mainDB).WithTx(tx)

	defer func() {
		if err != nil {
			if rollBackErr := tx.Rollback(); rollBackErr != nil {
				log.FromCtx(ctx).Error(err, "error rollback", rollBackErr)
				err = errors.WithStack(httpservice.ErrUnknownSource)

				return
			}
		}
	}()

	// Update Last login user
	if err = q.RecordUserLastLogin(ctx, user.Guid); err != nil {
		log.FromCtx(ctx).Error(err, "failed record last login")
		err = errors.WithStack(httpservice.ErrInternalService)

		return
	}

	// Update token auth record
	if err = q.RecordAuthTokenUserLogin(ctx, sqlc.RecordAuthTokenUserLoginParams{
		UserLogin: sql.NullString{
			String: user.Guid,
			Valid:  true,
		},
		Name:       jwtRequest.AppName,
		DeviceID:   jwtRequest.DeviceID,
		DeviceType: jwtRequest.DeviceType,
	}); err != nil {
		log.FromCtx(ctx).Error(err, "failed update token auth login user")
		err = errors.WithStack(httpservice.ErrInternalService)

		return
	}

	if err = tx.Commit(); err != nil {
		log.FromCtx(ctx).Error(err, "error commit")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	return
}

func (s *AuthorizationUserService) Logout(ctx context.Context, request jwt.RequestJWTToken) (err error) {
	tx, err := s.mainDB.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed begin tx")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	q := sqlc.New(s.mainDB).WithTx(tx)

	defer func() {
		if err != nil {
			if rollBackErr := tx.Rollback(); rollBackErr != nil {
				log.FromCtx(ctx).Error(err, "error rollback", rollBackErr)
				err = errors.WithStack(httpservice.ErrUnknownSource)

				return
			}
		}
	}()

	if err = q.ClearAuthTokenUserLogin(ctx, sqlc.ClearAuthTokenUserLoginParams{
		Name:       request.AppName,
		DeviceID:   request.DeviceID,
		DeviceType: request.DeviceType,
	}); err != nil {
		log.FromCtx(ctx).Error(err, "failed clear auth user login")
		err = errors.WithStack(err)

		return
	}

	if err = tx.Commit(); err != nil {
		log.FromCtx(ctx).Error(err, "error commit")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	return
}
