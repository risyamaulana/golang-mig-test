package service

import (
	"database/sql"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
)

type AuthorizationUserService struct {
	mainDB *sql.DB
	cfg    config.KVStore
}

func NewAuthorizationUserService(
	mainDB *sql.DB,
	cfg config.KVStore,
) *AuthorizationUserService {
	return &AuthorizationUserService{
		mainDB: mainDB,
		cfg:    cfg,
	}
}
