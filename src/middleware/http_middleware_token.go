package middleware

import (
	"database/sql"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/common/jwt"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"net/http"
)

type EnsureToken struct {
	mainDB *sql.DB
	config config.KVStore
}

func NewEnsureToken(db *sql.DB, cfg config.KVStore) *EnsureToken {
	return &EnsureToken{
		mainDB: db,
		config: cfg,
	}
}

func (v *EnsureToken) ValidateToken(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		request := ctx.Request()

		headerDataToken := request.Header.Get(v.config.GetString("header.token-param"))
		if headerDataToken == "" {
			return echo.NewHTTPError(http.StatusUnauthorized, httpservice.MsgHeaderTokenNotFound).SetInternal(errors.Wrap(httpservice.ErrMissingHeaderTokenData, httpservice.MsgHeaderTokenNotFound))
		}

		jwtResponse, err := jwt.ClaimsJwtToken(v.config, headerDataToken)
		if err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, httpservice.MsgHeaderTokenUnauthorized).SetInternal(errors.Wrap(err, httpservice.MsgHeaderTokenUnauthorized))
		}

		// Set data jwt response to ...
		ctx.Set("token-data", jwtResponse)

		return next(ctx)
	}
}

func (v *EnsureToken) ValidateRefreshToken(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		request := ctx.Request()

		headerDataToken := request.Header.Get(v.config.GetString("header.refresh-token-param"))
		if headerDataToken == "" {
			return echo.NewHTTPError(http.StatusUnauthorized, httpservice.MsgHeaderRefreshTokenNotFound).SetInternal(errors.Wrap(httpservice.ErrMissingHeaderRefreshTokenData, httpservice.MsgHeaderRefreshTokenNotFound))
		}

		jwtResponse, err := jwt.ClaimsJwtToken(v.config, headerDataToken)
		if err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, httpservice.MsgHeaderRefreshTokenUnauthorized).SetInternal(errors.Wrap(err, httpservice.MsgHeaderRefreshTokenUnauthorized))
		}

		// Set data jwt response to ...
		ctx.Set("token-data", jwtResponse)

		return next(ctx)
	}
}

func (v *EnsureToken) ValidateUserLogin(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// Get data token session
		tokenAuth := ctx.Get("token-data").(jwt.RequestJWTToken)

		q := sqlc.New(v.mainDB)

		token, err := q.GetAuthToken(ctx.Request().Context(), sqlc.GetAuthTokenParams{
			Name:       tokenAuth.AppName,
			DeviceID:   tokenAuth.DeviceID,
			DeviceType: tokenAuth.DeviceType,
		})
		if err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, httpservice.MsgHeaderTokenUnauthorized).SetInternal(errors.Wrap(httpservice.ErrInvalidToken, httpservice.MsgHeaderTokenUnauthorized))
		}

		if !token.IsLogin {
			return echo.NewHTTPError(http.StatusUnauthorized, httpservice.MsgIsNotLogin).SetInternal(errors.WithMessage(httpservice.ErrDataUserUnauthorized, httpservice.MsgIsNotLogin))
		}

		// Get user
		user, err := q.GetUser(ctx.Request().Context(), token.UserLogin.String)
		if err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, httpservice.MsgUserNotFound).SetInternal(errors.Wrap(httpservice.ErrDataUserUnauthorized, httpservice.MsgUserNotFound))
		}

		// check active user {
		if !user.IsActive {
			return echo.NewHTTPError(http.StatusUnauthorized, httpservice.MsgUserNotActive).SetInternal(errors.WithMessage(httpservice.ErrDataUserUnauthorized, httpservice.MsgUserNotActive))
		}

		// Set data user response to ...
		ctx.Set("user", user)

		return next(ctx)
	}
}
