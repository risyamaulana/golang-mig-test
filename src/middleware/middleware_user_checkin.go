package middleware

import (
	"database/sql"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"

	userAttendanceService "gitlab.com/risyamaulana/golang-mig-test/src/user_attendance/service"
)

type EnsureCheckin struct {
	mainDB *sql.DB
	config config.KVStore
}

func NewEnsureCheckin(db *sql.DB, cfg config.KVStore) *EnsureCheckin {
	return &EnsureCheckin{
		mainDB: db,
		config: cfg,
	}
}

func (v *EnsureCheckin) ValidateUserCheckin(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// service
		userAttendanceSvc := userAttendanceService.NewUserAttendanceService(v.mainDB, v.config)

		user := ctx.Get("user").(sqlc.MigUser)

		// check user is checkin
		attendance, err := userAttendanceSvc.GetUserAttendanceByDate(ctx.Request().Context(), user)
		if err != nil {
			return errors.WithStack(httpservice.ErrUserCheckin)
		}

		// check user is checkout
		if attendance.CheckOut.Valid {
			return errors.WithStack(httpservice.ErrUserAlreadyCheckout)
		}

		ctx.Set("user-attendance", attendance)

		return next(ctx)
	}
}
