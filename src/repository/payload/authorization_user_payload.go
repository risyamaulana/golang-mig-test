package payload

import (
	"github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
)

type AuthorizationUserPayload struct {
	Email    string `json:"email" valid:"required,email"`
	Password string `json:"password" valid:"required,minstringlength(8)"`
}

func (payload *AuthorizationUserPayload) Validate() (err error) {
	// Validate Payload
	if _, err = govalidator.ValidateStruct(payload); err != nil {
		err = errors.Wrapf(httpservice.ErrBadRequest, "bad request: %s", err.Error())
		return
	}

	return
}
