package payload

import (
	"github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"time"
)

type CreateUserActivitiesPayload struct {
	Activities []string `json:"activities" valid:"required"`
}

type UpdateUserActivityPayload struct {
	Activity string `json:"activity" valid:"required"`
}

type DeleteBulkUserActivityPayload struct {
	IDs []int64 `json:"ids" valid:"required"`
}

type ListUserActivityPayload struct {
	AttendanceDateFrom time.Time `json:"attendance_date_from" valid:"required"`
	AttendanceDateTo   time.Time `json:"attendance_date_to" valid:"required"'`
}

type ReadUserActivitiesPayload struct {
	User       ReadUserPayload               `json:"user"`
	Attendance ReadUserAttendanceDataPayload `json:"attendance"`
	Activities []*ReadUserActivityPayload    `json:"activities"`
}

type ReadUserActivityPayload struct {
	ID           int64      `json:"id"`
	AttendanceID int64      `json:"-"`
	Activity     string     `json:"activity"`
	CreatedAt    time.Time  `json:"created_at"`
	UpdatedAt    *time.Time `json:"updated_at"`
}

type ReadListUserActivityPayload struct {
	User     ReadUserPayload                   `json:"user"`
	ListData []ReadListUserActivityDataPayload `json:"list_data"`
}

type ReadListUserActivityDataPayload struct {
	Attendance ReadUserAttendanceDataPayload `json:"attendance"`
	Activities []ReadUserActivityPayload     `json:"activities"`
}

func (payload *CreateUserActivitiesPayload) Validate() (err error) {
	// Validate Payload
	if _, err = govalidator.ValidateStruct(payload); err != nil {
		err = errors.Wrapf(httpservice.ErrBadRequest, "bad request: %s", err.Error())
		return
	}

	return
}

func (payload *UpdateUserActivityPayload) Validate() (err error) {
	// Validate Payload
	if _, err = govalidator.ValidateStruct(payload); err != nil {
		err = errors.Wrapf(httpservice.ErrBadRequest, "bad request: %s", err.Error())
		return
	}

	return
}

func (payload *DeleteBulkUserActivityPayload) Validate() (err error) {
	// Validate Payload
	if _, err = govalidator.ValidateStruct(payload); err != nil {
		err = errors.Wrapf(httpservice.ErrBadRequest, "bad request: %s", err.Error())
		return
	}

	return
}

func (payload *ListUserActivityPayload) Validate() (err error) {
	// Validate Payload
	if _, err = govalidator.ValidateStruct(payload); err != nil {
		err = errors.Wrapf(httpservice.ErrBadRequest, "bad request: %s", err.Error())
		return
	}

	return
}

func (payload *UpdateUserActivityPayload) ToEntity(id int64) (data sqlc.UpdateUserActivityParams) {
	data = sqlc.UpdateUserActivityParams{
		Activity: payload.Activity,
		ID:       id,
	}

	return
}

func (payload *ListUserActivityPayload) ToEntity(user sqlc.MigUser) (data sqlc.ListUserActivityParams) {
	data = sqlc.ListUserActivityParams{
		AttendanceDateFrom: payload.AttendanceDateFrom.Local(),
		AttendanceDateTo:   payload.AttendanceDateTo.Local(),
		UserGuid:           user.Guid,
	}

	return
}

func ToPayloadUserActivity(data sqlc.MigUserActivity) (payload ReadUserActivityPayload) {
	payload = ReadUserActivityPayload{
		ID:           data.ID,
		AttendanceID: data.AttendanceID,
		Activity:     data.Activity,
		CreatedAt:    data.CreatedAt,
	}

	if data.UpdatedAt.Valid {
		updatedAt := data.UpdatedAt.Time
		payload.UpdatedAt = &updatedAt
	}

	return
}

func ToPayloadCreateActivities(cfg config.KVStore, data []sqlc.MigUserActivity, user sqlc.MigUser, attendance sqlc.MigUsersAttendance) (payload ReadUserActivitiesPayload) {
	payload = ReadUserActivitiesPayload{
		User:       ToPayloadUser(user),
		Attendance: ToPayloadUserAttendanceData(cfg, attendance),
		Activities: nil,
	}

	activities := make([]*ReadUserActivityPayload, len(data))
	for i := range data {
		activities[i] = new(ReadUserActivityPayload)
		activity := ToPayloadUserActivity(data[i])
		activities[i] = &activity
	}

	payload.Activities = activities

	return
}

func ToPayloadListActivity(cfg config.KVStore, data []sqlc.ListUserActivityRow, user sqlc.MigUser) (payload ReadListUserActivityPayload) {
	payload = ReadListUserActivityPayload{
		User:     ToPayloadUser(user),
		ListData: nil,
	}

	listUserActivityData := make(map[time.Time]*ReadListUserActivityDataPayload)
	for i := range data {
		if _, ok := listUserActivityData[data[i].AttendanceDate.Time]; !ok {
			listUserActivityData[data[i].AttendanceDate.Time] = new(ReadListUserActivityDataPayload)
			attendance := ToPayloadUserAttendanceData(cfg, sqlc.MigUsersAttendance{
				ID:             data[i].AttendanceID.Int64,
				UserGuid:       user.Guid,
				AttendanceDate: data[i].AttendanceDate.Time,
				CheckIn:        data[i].CheckIn.Time,
				CheckOut:       data[i].CheckOut,
			})

			activity := ToPayloadUserActivity(sqlc.MigUserActivity{
				ID:           data[i].ActivityID,
				AttendanceID: data[i].AttendanceID.Int64,
				Activity:     data[i].Activity,
				CreatedAt:    data[i].CreatedAt,
				UpdatedAt:    data[i].UpdatedAt,
			})

			listUserActivityData[data[i].AttendanceDate.Time].Attendance = attendance

			listUserActivityData[data[i].AttendanceDate.Time].Activities = append(listUserActivityData[data[i].AttendanceDate.Time].Activities, activity)
		} else {
			activity := ToPayloadUserActivity(sqlc.MigUserActivity{
				ID:           data[i].ActivityID,
				AttendanceID: data[i].AttendanceID.Int64,
				Activity:     data[i].Activity,
				CreatedAt:    data[i].CreatedAt,
				UpdatedAt:    data[i].UpdatedAt,
			})
			listUserActivityData[data[i].AttendanceDate.Time].Activities = append(listUserActivityData[data[i].AttendanceDate.Time].Activities, activity)
		}
	}

	for _, activity := range listUserActivityData {
		payload.ListData = append(payload.ListData, *activity)
	}

	return
}
