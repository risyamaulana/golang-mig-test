package payload

import (
	"github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/common/utility"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"time"
)

type ListUserAttendancePayload struct {
	AttendanceDateFrom time.Time `json:"attendance_date_from" valid:"required"`
	AttendanceDateTo   time.Time `json:"attendance_date_to" valid:"required"'`
}

type ReadUserAttendancePayload struct {
	UserData   ReadUserPayload                  `json:"user"`
	Attendance []*ReadUserAttendanceDataPayload `json:"attendance"`
}

type ReadUserAttendanceDataPayload struct {
	AttendanceDate time.Time `json:"attendance_date"`
	CheckIn        string    `json:"check_in"`
	CheckOut       *string   `json:"check_out"`
}

func (payload *ListUserAttendancePayload) Validate() (err error) {
	// Validate Payload
	if _, err = govalidator.ValidateStruct(payload); err != nil {
		err = errors.Wrapf(httpservice.ErrBadRequest, "bad request: %s", err.Error())
		return
	}

	return
}

func (payload *ListUserAttendancePayload) ToEntity(user sqlc.MigUser) (data sqlc.ListUserAttendanceParams) {
	data = sqlc.ListUserAttendanceParams{
		AttendanceDateFrom: payload.AttendanceDateFrom.Local(),
		AttendanceDateTo:   payload.AttendanceDateTo.Local(),
		UserGuid:           user.Guid,
	}

	return
}

func ToPayloadUserAttendance(cfg config.KVStore, user sqlc.MigUser, attendances []sqlc.MigUsersAttendance) (payload ReadUserAttendancePayload) {
	payload = ReadUserAttendancePayload{
		UserData: ToPayloadUser(user),
	}

	attendancesPayload := make([]*ReadUserAttendanceDataPayload, len(attendances))
	for i := range attendances {
		attendancesPayload[i] = new(ReadUserAttendanceDataPayload)
		attendance := ToPayloadUserAttendanceData(cfg, attendances[i])

		attendancesPayload[i] = &attendance
	}

	payload.Attendance = attendancesPayload

	return
}

func ToPayloadUserAttendanceData(cfg config.KVStore, attendance sqlc.MigUsersAttendance) (payload ReadUserAttendanceDataPayload) {
	payload = ReadUserAttendanceDataPayload{
		AttendanceDate: attendance.AttendanceDate.Local(),
		CheckIn:        utility.ParseTimestampToTime(cfg, attendance.CheckIn),
		CheckOut:       nil,
	}

	if attendance.CheckOut.Valid {
		checkout := utility.ParseTimestampToTime(cfg, attendance.CheckOut.Time)
		payload.CheckOut = &checkout
	}

	return
}
