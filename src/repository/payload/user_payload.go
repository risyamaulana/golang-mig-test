package payload

import (
	"database/sql"
	"github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/common/utility"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"time"
)

type UpdateUserPayload struct {
	Name    string  `json:"name" valid:"required"`
	Gender  string  `json:"gender" valid:"required"`
	Address *string `json:"address"`
}

type UpdateUserPasswordPayload struct {
	OldPassword        string `json:"old_password" valid:"required,minstringlength(8)"`
	NewPassword        string `json:"new_password" valid:"required,minstringlength(8)"`
	ConfirmNewPassword string `json:"confirm_new_password" valid:"required,minstringlength(8)"`
}

type ReadUserPayload struct {
	ID        int64      `json:"-"`
	GUID      string     `json:"id"`
	Name      string     `json:"name"`
	Email     string     `json:"email"`
	Gender    string     `json:"gender"`
	Address   *string    `json:"address"`
	Salt      string     `json:"-"`
	Password  string     `json:"-"`
	IsActive  bool       `json:"is_active"`
	CreatedAt time.Time  `json:"created_at"`
	CreatedBy string     `json:"created_by"`
	UpdatedAt *time.Time `json:"updated_at"`
	LastLogin *time.Time `json:"-"`
}

func (payload *UpdateUserPayload) Validate() (err error) {
	// Validate Payload
	if _, err = govalidator.ValidateStruct(payload); err != nil {
		err = errors.Wrapf(httpservice.ErrBadRequest, "bad request: %s", err.Error())
		return
	}

	return
}

func (payload *UpdateUserPasswordPayload) Validate(userData sqlc.MigUser) (err error) {
	// Validate Payload
	if _, err = govalidator.ValidateStruct(payload); err != nil {
		err = errors.Wrapf(httpservice.ErrBadRequest, "bad request: %s", err.Error())
		return
	}

	// Validate Old Password
	password := utility.GeneratePassword(userData.Salt, payload.OldPassword)
	if userData.Password != password {
		err = errors.WithStack(httpservice.ErrPasswordNotMatch)
		return
	}

	// Validate confirm password
	if payload.NewPassword != payload.ConfirmNewPassword {
		err = errors.WithStack(httpservice.ErrConfirmPasswordNotMatch)
		return
	}

	return
}

func (payload *UpdateUserPayload) ToEntity(user sqlc.MigUser) (data sqlc.UpdateUserParams) {
	data = sqlc.UpdateUserParams{
		Name:   payload.Name,
		Gender: payload.Gender,
		Guid:   user.Guid,
	}

	if payload.Address != nil {
		data.Address = sql.NullString{
			String: *payload.Address,
			Valid:  true,
		}
	}

	return
}

func (payload *UpdateUserPasswordPayload) ToEntity(cfg config.KVStore, user sqlc.MigUser) (data sqlc.UpdateUserPasswordParams) {
	// Generate Salt & Password
	salt := utility.GeneratePasswordSalt(cfg)
	password := utility.GeneratePassword(salt, payload.NewPassword)

	data = sqlc.UpdateUserPasswordParams{
		Password: password,
		Salt:     salt,
		Guid:     user.Guid,
	}

	return
}

func ToPayloadUser(data sqlc.MigUser) (payload ReadUserPayload) {
	payload = ReadUserPayload{
		ID:        data.ID,
		GUID:      data.Guid,
		Name:      data.Name,
		Email:     data.Email,
		Gender:    data.Gender,
		Salt:      data.Salt,
		Password:  data.Password,
		IsActive:  data.IsActive,
		CreatedAt: data.CreatedAt,
		CreatedBy: data.CreatedBy,
	}

	if data.Address.Valid {
		payload.Address = &data.Address.String
	}

	if data.UpdatedAt.Valid {
		updateAt := data.UpdatedAt.Time
		payload.UpdatedAt = &updateAt
	}

	if data.LastLogin.Valid {
		lastLogin := data.UpdatedAt.Time
		payload.LastLogin = &lastLogin
	}

	return
}
