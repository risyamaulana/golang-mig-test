// Code generated by sqlc. DO NOT EDIT.

package sqlc

import (
	"database/sql"
	"time"
)

type MigAppKey struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
	Key  string `json:"key"`
}

type MigAuthToken struct {
	ID                  int64          `json:"id"`
	Name                string         `json:"name"`
	DeviceID            string         `json:"device_id"`
	DeviceType          string         `json:"device_type"`
	Token               string         `json:"token"`
	TokenExpired        time.Time      `json:"token_expired"`
	RefreshToken        string         `json:"refresh_token"`
	RefreshTokenExpired time.Time      `json:"refresh_token_expired"`
	IsLogin             bool           `json:"is_login"`
	UserLogin           sql.NullString `json:"user_login"`
	CreatedAt           time.Time      `json:"created_at"`
	UpdatedAt           sql.NullTime   `json:"updated_at"`
}

type MigUser struct {
	ID        int64          `json:"id"`
	Guid      string         `json:"guid"`
	Name      string         `json:"name"`
	Email     string         `json:"email"`
	Gender    string         `json:"gender"`
	Address   sql.NullString `json:"address"`
	Salt      string         `json:"salt"`
	Password  string         `json:"password"`
	IsActive  bool           `json:"is_active"`
	CreatedAt time.Time      `json:"created_at"`
	CreatedBy string         `json:"created_by"`
	UpdatedAt sql.NullTime   `json:"updated_at"`
	LastLogin sql.NullTime   `json:"last_login"`
}

type MigUserActivity struct {
	ID           int64        `json:"id"`
	AttendanceID int64        `json:"attendance_id"`
	Activity     string       `json:"activity"`
	CreatedAt    time.Time    `json:"created_at"`
	UpdatedAt    sql.NullTime `json:"updated_at"`
	DeletedAt    sql.NullTime `json:"deleted_at"`
}

type MigUsersAttendance struct {
	ID             int64        `json:"id"`
	UserGuid       string       `json:"user_guid"`
	AttendanceDate time.Time    `json:"attendance_date"`
	CheckIn        time.Time    `json:"check_in"`
	CheckOut       sql.NullTime `json:"check_out"`
}
