// Code generated by sqlc. DO NOT EDIT.
// source: user.sql

package sqlc

import (
	"context"
	"database/sql"
)

const getUser = `-- name: GetUser :one
SELECT
    mu.id, mu.guid, mu.name, mu.email, mu.gender, mu.address, mu.salt, mu.password, mu.is_active, mu.created_at, mu.created_by, mu.updated_at, mu.last_login
FROM mig_users mu
WHERE
    mu.guid = $1
`

func (q *Queries) GetUser(ctx context.Context, guid string) (MigUser, error) {
	row := q.db.QueryRowContext(ctx, getUser, guid)
	var i MigUser
	err := row.Scan(
		&i.ID,
		&i.Guid,
		&i.Name,
		&i.Email,
		&i.Gender,
		&i.Address,
		&i.Salt,
		&i.Password,
		&i.IsActive,
		&i.CreatedAt,
		&i.CreatedBy,
		&i.UpdatedAt,
		&i.LastLogin,
	)
	return i, err
}

const getUserByEmail = `-- name: GetUserByEmail :one
SELECT
    mu.id, mu.guid, mu.name, mu.email, mu.gender, mu.address, mu.salt, mu.password, mu.is_active, mu.created_at, mu.created_by, mu.updated_at, mu.last_login
FROM mig_users mu
WHERE
    mu.email = $1
`

func (q *Queries) GetUserByEmail(ctx context.Context, email string) (MigUser, error) {
	row := q.db.QueryRowContext(ctx, getUserByEmail, email)
	var i MigUser
	err := row.Scan(
		&i.ID,
		&i.Guid,
		&i.Name,
		&i.Email,
		&i.Gender,
		&i.Address,
		&i.Salt,
		&i.Password,
		&i.IsActive,
		&i.CreatedAt,
		&i.CreatedBy,
		&i.UpdatedAt,
		&i.LastLogin,
	)
	return i, err
}

const recordUserLastLogin = `-- name: RecordUserLastLogin :exec
UPDATE mig_users
SET
    last_login = (now() at time zone 'UTC')::TIMESTAMP
WHERE
    guid = $1
`

func (q *Queries) RecordUserLastLogin(ctx context.Context, guid string) error {
	_, err := q.db.ExecContext(ctx, recordUserLastLogin, guid)
	return err
}

const updateUser = `-- name: UpdateUser :one
UPDATE mig_users
SET
    name = $1,
    gender = $2,
    address = $3,
    updated_at = (now() at time zone 'UTC')::TIMESTAMP
WHERE
    guid = $4
RETURNING mig_users.id, mig_users.guid, mig_users.name, mig_users.email, mig_users.gender, mig_users.address, mig_users.salt, mig_users.password, mig_users.is_active, mig_users.created_at, mig_users.created_by, mig_users.updated_at, mig_users.last_login
`

type UpdateUserParams struct {
	Name    string         `json:"name"`
	Gender  string         `json:"gender"`
	Address sql.NullString `json:"address"`
	Guid    string         `json:"guid"`
}

func (q *Queries) UpdateUser(ctx context.Context, arg UpdateUserParams) (MigUser, error) {
	row := q.db.QueryRowContext(ctx, updateUser,
		arg.Name,
		arg.Gender,
		arg.Address,
		arg.Guid,
	)
	var i MigUser
	err := row.Scan(
		&i.ID,
		&i.Guid,
		&i.Name,
		&i.Email,
		&i.Gender,
		&i.Address,
		&i.Salt,
		&i.Password,
		&i.IsActive,
		&i.CreatedAt,
		&i.CreatedBy,
		&i.UpdatedAt,
		&i.LastLogin,
	)
	return i, err
}

const updateUserPassword = `-- name: UpdateUserPassword :exec
UPDATE mig_users
SET
    salt = $1,
    password = $2,
    updated_at = (now() at time zone 'UTC')::TIMESTAMP
WHERE
    guid = $3
`

type UpdateUserPasswordParams struct {
	Salt     string `json:"salt"`
	Password string `json:"password"`
	Guid     string `json:"guid"`
}

func (q *Queries) UpdateUserPassword(ctx context.Context, arg UpdateUserPasswordParams) error {
	_, err := q.db.ExecContext(ctx, updateUserPassword, arg.Salt, arg.Password, arg.Guid)
	return err
}
