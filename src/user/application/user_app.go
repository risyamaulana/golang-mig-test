package application

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/src/middleware"
	"gitlab.com/risyamaulana/golang-mig-test/src/repository/payload"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/src/user/service"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
	"net/http"
)

func AddRouteUser(s *httpservice.Service, cfg config.KVStore, e *echo.Echo) {
	svc := service.NewUserService(s.GetDB(), cfg)

	mddw := middleware.NewEnsureToken(s.GetDB(), cfg)
	user := e.Group("/user")
	user.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "user ok")
	})
	user.Use(mddw.ValidateToken)

	userProfile := user.Group("/profile", mddw.ValidateUserLogin)
	userProfile.GET("", getMyProfile(svc))
	userProfile.PUT("", updateMyProfile(svc))
	userProfile.PUT("/change-password", updateMyPassword(svc, cfg))
}

func getMyProfile(svc *service.UserService) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		data, err := svc.GetUser(ctx.Request().Context(), ctx.Get("user").(sqlc.MigUser).Guid)
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, payload.ToPayloadUser(data), nil)
	}
}

func updateMyProfile(svc *service.UserService) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		var request payload.UpdateUserPayload
		if err := ctx.Bind(&request); err != nil {
			log.FromCtx(ctx.Request().Context()).Error(err, "failed to parse request")
			return errors.WithStack(httpservice.ErrBadRequest)
		}

		// Validate request
		if err := request.Validate(); err != nil {
			return err
		}

		data, err := svc.UpdateUser(ctx.Request().Context(), request.ToEntity(ctx.Get("user").(sqlc.MigUser)))
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, payload.ToPayloadUser(data), nil)
	}
}

func updateMyPassword(svc *service.UserService, cfg config.KVStore) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		var request payload.UpdateUserPasswordPayload
		if err := ctx.Bind(&request); err != nil {
			log.FromCtx(ctx.Request().Context()).Error(err, "failed to parse request")
			return errors.WithStack(httpservice.ErrBadRequest)
		}

		// Validate request
		if err := request.Validate(ctx.Get("user").(sqlc.MigUser)); err != nil {
			return err
		}

		err := svc.UpdateUserPassword(ctx.Request().Context(), request.ToEntity(cfg, ctx.Get("user").(sqlc.MigUser)))
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, nil, nil)
	}
}
