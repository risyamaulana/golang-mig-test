package service

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
)

func (s *UserService) GetUser(ctx context.Context, guid string) (user sqlc.MigUser, err error) {
	q := sqlc.New(s.mainDB)

	user, err = q.GetUser(ctx, guid)
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed get data user")
		err = errors.WithStack(httpservice.ErrDataUserNotFound)

		return
	}

	return
}

func (s *UserService) GetUserByEmail(ctx context.Context, email string) (user sqlc.MigUser, err error) {
	q := sqlc.New(s.mainDB)

	user, err = q.GetUserByEmail(ctx, email)
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed get data user")
		err = errors.WithStack(httpservice.ErrDataUserNotFound)

		return
	}

	return
}
