package service

import (
	"database/sql"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
)

type UserService struct {
	mainDB *sql.DB
	cfg    config.KVStore
}

func NewUserService(
	mainDB *sql.DB,
	cfg config.KVStore,
) *UserService {
	return &UserService{
		mainDB: mainDB,
		cfg:    cfg,
	}
}
