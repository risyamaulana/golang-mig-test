package application

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/src/middleware"
	"gitlab.com/risyamaulana/golang-mig-test/src/repository/payload"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/src/user_activity/service"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
	"net/http"
	"strconv"
)

const (
	baseDecimalDefault = 10
	bitSizeDefault     = 64
)

func AddRouteUserActivity(s *httpservice.Service, cfg config.KVStore, e *echo.Echo) {
	svc := service.NewUserActivityService(s.GetDB(), cfg)

	mddw := middleware.NewEnsureToken(s.GetDB(), cfg)
	userCheckinMddw := middleware.NewEnsureCheckin(s.GetDB(), cfg)

	userActivity := e.Group("/user/activity")
	userActivity.Use(mddw.ValidateToken)
	userActivity.Use(mddw.ValidateUserLogin)

	userActivity.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "user activity ok")
	})

	userActivity.POST("", createUserActivities(svc, cfg), userCheckinMddw.ValidateUserCheckin)
	userActivity.PUT("/:id", updateUserActivity(svc), userCheckinMddw.ValidateUserCheckin)
	userActivity.DELETE("", deleteUserActivity(svc), userCheckinMddw.ValidateUserCheckin)
	userActivity.GET("", getUserActivities(svc, cfg), userCheckinMddw.ValidateUserCheckin)
	userActivity.POST("/list", listUserActivity(svc, cfg))

}

func createUserActivities(svc *service.UserActivityService, cfg config.KVStore) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		var request payload.CreateUserActivitiesPayload
		if err := ctx.Bind(&request); err != nil {
			log.FromCtx(ctx.Request().Context()).Error(err, "failed to parse request")
			return errors.WithStack(httpservice.ErrBadRequest)
		}

		// Validate request
		if err := request.Validate(); err != nil {
			return err
		}

		activities, err := svc.CreateUserActivities(ctx.Request().Context(), ctx.Get("user-attendance").(sqlc.MigUsersAttendance), request.Activities)
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, payload.ToPayloadCreateActivities(cfg, activities, ctx.Get("user").(sqlc.MigUser), ctx.Get("user-attendance").(sqlc.MigUsersAttendance)), nil)
	}
}

func updateUserActivity(svc *service.UserActivityService) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		id, err := strconv.ParseInt(ctx.Param("id"), baseDecimalDefault, bitSizeDefault)
		if err != nil {
			return errors.Wrap(httpservice.ErrBadRequest, "failed to parse user handheld id param")
		}

		var request payload.UpdateUserActivityPayload
		if err := ctx.Bind(&request); err != nil {
			log.FromCtx(ctx.Request().Context()).Error(err, "failed to parse request")
			return errors.WithStack(httpservice.ErrBadRequest)
		}

		// Validate request
		if err := request.Validate(); err != nil {
			return err
		}

		activity, err := svc.UpdateUserActivity(ctx.Request().Context(), request.ToEntity(id))
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, payload.ToPayloadUserActivity(activity), nil)
	}
}

func deleteUserActivity(svc *service.UserActivityService) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		var request payload.DeleteBulkUserActivityPayload
		if err := ctx.Bind(&request); err != nil {
			log.FromCtx(ctx.Request().Context()).Error(err, "failed to parse request")
			return errors.WithStack(httpservice.ErrBadRequest)
		}

		// Validate request
		if err := request.Validate(); err != nil {
			return err
		}

		if err := svc.DeleteUserActivities(ctx.Request().Context(), request.IDs); err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, nil, nil)
	}
}

func listUserActivity(svc *service.UserActivityService, cfg config.KVStore) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		var request payload.ListUserActivityPayload
		if err := ctx.Bind(&request); err != nil {
			log.FromCtx(ctx.Request().Context()).Error(err, "failed to parse request")
			return errors.WithStack(httpservice.ErrBadRequest)
		}

		// Validate request
		if err := request.Validate(); err != nil {
			return err
		}

		activities, err := svc.ListUserActivity(ctx.Request().Context(), request.ToEntity(ctx.Get("user").(sqlc.MigUser)))
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, payload.ToPayloadListActivity(cfg, activities, ctx.Get("user").(sqlc.MigUser)), nil)
	}
}

func getUserActivities(svc *service.UserActivityService, cfg config.KVStore) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		activities, err := svc.GetUserActivities(ctx.Request().Context(), ctx.Get("user-attendance").(sqlc.MigUsersAttendance))
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, payload.ToPayloadCreateActivities(cfg, activities, ctx.Get("user").(sqlc.MigUser), ctx.Get("user-attendance").(sqlc.MigUsersAttendance)), nil)
	}
}
