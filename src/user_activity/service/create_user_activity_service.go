package service

import (
	"context"
	"database/sql"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
)

func (s *UserActivityService) CreateUserActivities(ctx context.Context, attendance sqlc.MigUsersAttendance, activities []string) (userActivities []sqlc.MigUserActivity, err error) {
	tx, err := s.mainDB.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed begin tx")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	q := sqlc.New(s.mainDB).WithTx(tx)

	defer func() {
		if err != nil {
			if rollBackErr := tx.Rollback(); rollBackErr != nil {
				log.FromCtx(ctx).Error(err, "error rollback", rollBackErr)
				err = errors.WithStack(httpservice.ErrUnknownSource)

				return
			}
		}
	}()

	userActivities = make([]sqlc.MigUserActivity, len(activities))
	for i := range activities {
		userActivity, errInsert := s.insertUserActivity(ctx, q, attendance, activities[i])
		if errInsert != nil {
			err = errInsert
			return
		}

		userActivities[i] = userActivity
	}

	if err = tx.Commit(); err != nil {
		log.FromCtx(ctx).Error(err, "error commit")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	return
}

func (s *UserActivityService) insertUserActivity(ctx context.Context, q *sqlc.Queries, attendance sqlc.MigUsersAttendance, activity string) (userActivity sqlc.MigUserActivity, err error) {
	userActivity, err = q.InsertUserActivity(ctx, sqlc.InsertUserActivityParams{
		AttendanceID: attendance.ID,
		Activity:     activity,
	})

	if err != nil {
		err = errors.Wrap(err, "failed insert user activity")
	}

	return
}
