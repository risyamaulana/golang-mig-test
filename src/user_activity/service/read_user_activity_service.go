package service

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
)

func (s *UserActivityService) ListUserActivity(ctx context.Context, request sqlc.ListUserActivityParams) (activities []sqlc.ListUserActivityRow, err error) {
	q := sqlc.New(s.mainDB)

	activities, err = q.ListUserActivity(ctx, request)
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed get list activity")
		err = errors.WithStack(httpservice.ErrDataNotFound)

		return
	}

	return
}

func (s *UserActivityService) GetUserActivities(ctx context.Context, attendance sqlc.MigUsersAttendance) (activities []sqlc.MigUserActivity, err error) {
	q := sqlc.New(s.mainDB)

	activities, err = q.GetUserActivitiesByAttendance(ctx, attendance.ID)
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed get activities by attendance")
		err = errors.WithStack(httpservice.ErrDataNotFound)

		return
	}

	return
}
