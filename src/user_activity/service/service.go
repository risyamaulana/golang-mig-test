package service

import (
	"database/sql"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
)

type UserActivityService struct {
	mainDB *sql.DB
	cfg    config.KVStore
}

func NewUserActivityService(
	mainDB *sql.DB,
	cfg config.KVStore,
) *UserActivityService {
	return &UserActivityService{
		mainDB: mainDB,
		cfg:    cfg,
	}
}
