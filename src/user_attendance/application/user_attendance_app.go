package application

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/src/middleware"
	"gitlab.com/risyamaulana/golang-mig-test/src/repository/payload"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/src/user_attendance/service"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
	"net/http"
)

func AddRouteUserAttendance(s *httpservice.Service, cfg config.KVStore, e *echo.Echo) {
	svc := service.NewUserAttendanceService(s.GetDB(), cfg)

	mddw := middleware.NewEnsureToken(s.GetDB(), cfg)
	userAttendance := e.Group("/user/attendance")
	userAttendance.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "user attendance ok")
	})
	userAttendance.Use(mddw.ValidateToken)
	userAttendance.Use(mddw.ValidateUserLogin)

	userAttendance.POST("/check-in", userAttendanceCheckin(svc))
	userAttendance.POST("/check-out", userAttendanceCheckout(svc))
	userAttendance.POST("/list", listUserAttendance(svc, cfg))
}

func userAttendanceCheckin(svc *service.UserAttendanceService) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if err := svc.RecordUserCheckin(ctx.Request().Context(), ctx.Get("user").(sqlc.MigUser)); err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, nil, nil)
	}
}

func userAttendanceCheckout(svc *service.UserAttendanceService) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if err := svc.RecordUserCheckout(ctx.Request().Context(), ctx.Get("user").(sqlc.MigUser)); err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, nil, nil)
	}
}

func listUserAttendance(svc *service.UserAttendanceService, cfg config.KVStore) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		var request payload.ListUserAttendancePayload
		if err := ctx.Bind(&request); err != nil {
			log.FromCtx(ctx.Request().Context()).Error(err, "failed to parse request")
			return errors.WithStack(httpservice.ErrBadRequest)
		}

		// Validate request
		if err := request.Validate(); err != nil {
			return err
		}

		listAttendance, err := svc.ListUserAttendance(ctx.Request().Context(), request.ToEntity(ctx.Get("user").(sqlc.MigUser)))
		if err != nil {
			return err
		}

		return httpservice.ResponseData(ctx, payload.ToPayloadUserAttendance(cfg, ctx.Get("user").(sqlc.MigUser), listAttendance), nil)
	}
}
