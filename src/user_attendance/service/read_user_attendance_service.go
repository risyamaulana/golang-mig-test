package service

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	"gitlab.com/risyamaulana/golang-mig-test/common/utility"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
	"time"
)

func (s *UserAttendanceService) checkUserIsAlreadyCheckin(ctx context.Context, user sqlc.MigUser) (isCheckin bool) {
	q := sqlc.New(s.mainDB)

	date := utility.ParseTimeToDate(s.cfg, time.Now())
	fmt.Println("DATE FOR CHECK CHECKIN : ", date)
	if _, err := q.GetUserAttendanceByUserDate(ctx, sqlc.GetUserAttendanceByUserDateParams{
		UserGuid:       user.Guid,
		AttendanceDate: utility.ParseTimeToDate(s.cfg, time.Now()),
	}); err == nil {
		isCheckin = true
	}

	return
}

func (s *UserAttendanceService) checkUserIsAlreadyCheckout(ctx context.Context, user sqlc.MigUser) (attendance sqlc.MigUsersAttendance, isCheckout bool, err error) {
	q := sqlc.New(s.mainDB)

	attendance, err = q.GetUserAttendanceByUserDate(ctx, sqlc.GetUserAttendanceByUserDateParams{
		UserGuid:       user.Guid,
		AttendanceDate: utility.ParseTimeToDate(s.cfg, time.Now()),
	})
	if err != nil {
		err = errors.Wrapf(err, "failed get attendance by user=%s and date=%s ", user.Guid, utility.ParseTimeToDate(s.cfg, time.Now()))
		return
	}

	if attendance.CheckOut.Valid {
		isCheckout = true
	}

	return
}

func (s *UserAttendanceService) GetUserAttendanceByDate(ctx context.Context, user sqlc.MigUser) (attendance sqlc.MigUsersAttendance, err error) {
	q := sqlc.New(s.mainDB)

	attendance, err = q.GetUserAttendanceByUserDate(ctx, sqlc.GetUserAttendanceByUserDateParams{
		UserGuid:       user.Guid,
		AttendanceDate: utility.ParseTimeToDate(s.cfg, time.Now()),
	})
	if err != nil {
		err = errors.Wrapf(err, "failed get attendance by user=%s and date=%s ", user.Guid, utility.ParseTimeToDate(s.cfg, time.Now()))
		return
	}

	return
}

func (s *UserAttendanceService) ListUserAttendance(ctx context.Context, request sqlc.ListUserAttendanceParams) (attendances []sqlc.MigUsersAttendance, err error) {
	q := sqlc.New(s.mainDB)

	attendances, err = q.ListUserAttendance(ctx, request)
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed get list user attendance")
		err = errors.WithStack(httpservice.ErrInternalService)

		return
	}

	return
}
