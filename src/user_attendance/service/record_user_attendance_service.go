package service

import (
	"context"
	"database/sql"
	"github.com/pkg/errors"
	"gitlab.com/risyamaulana/golang-mig-test/common/httpservice"
	sqlc "gitlab.com/risyamaulana/golang-mig-test/src/repository/pgbo_sqlc"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/log"
)

func (s *UserAttendanceService) RecordUserCheckin(ctx context.Context, user sqlc.MigUser) (err error) {
	tx, err := s.mainDB.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed begin tx")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	q := sqlc.New(s.mainDB).WithTx(tx)

	defer func() {
		if err != nil {
			if rollBackErr := tx.Rollback(); rollBackErr != nil {
				log.FromCtx(ctx).Error(err, "error rollback", rollBackErr)
				err = errors.WithStack(httpservice.ErrUnknownSource)

				return
			}
		}
	}()

	// Check if user already checkin
	if s.checkUserIsAlreadyCheckin(ctx, user) {
		err = errors.WithStack(httpservice.ErrUserAlreadyCheckin)
		return
	}

	if err = q.RecordUserCheckin(ctx, user.Guid); err != nil {
		log.FromCtx(ctx).Error(err, "failed record user attendance (check-in)")
		err = errors.WithStack(httpservice.ErrInternalService)

		return
	}

	if err = tx.Commit(); err != nil {
		log.FromCtx(ctx).Error(err, "error commit")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	return
}

func (s *UserAttendanceService) RecordUserCheckout(ctx context.Context, user sqlc.MigUser) (err error) {
	tx, err := s.mainDB.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		log.FromCtx(ctx).Error(err, "failed begin tx")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	q := sqlc.New(s.mainDB).WithTx(tx)

	defer func() {
		if err != nil {
			if rollBackErr := tx.Rollback(); rollBackErr != nil {
				log.FromCtx(ctx).Error(err, "error rollback", rollBackErr)
				err = errors.WithStack(httpservice.ErrUnknownSource)

				return
			}
		}
	}()

	// Check if user already checkin
	userAttendance, isCheckout, err := s.checkUserIsAlreadyCheckout(ctx, user)
	if err != nil {
		err = errors.WithStack(httpservice.ErrUserCheckin)
		return
	}

	if isCheckout {
		err = errors.WithStack(httpservice.ErrUserAlreadyCheckout)
		return
	}

	if err = q.RecordUserCheckout(ctx, userAttendance.ID); err != nil {
		log.FromCtx(ctx).Error(err, "failed record user attendance (check-out)")
		err = errors.WithStack(httpservice.ErrInternalService)

		return
	}

	if err = tx.Commit(); err != nil {
		log.FromCtx(ctx).Error(err, "error commit")
		err = errors.WithStack(httpservice.ErrUnknownSource)

		return
	}

	return
}
