package service

import (
	"database/sql"
	"gitlab.com/risyamaulana/golang-mig-test/toolkit/config"
)

type UserAttendanceService struct {
	mainDB *sql.DB
	cfg    config.KVStore
}

func NewUserAttendanceService(
	mainDB *sql.DB,
	cfg config.KVStore,
) *UserAttendanceService {
	return &UserAttendanceService{
		mainDB: mainDB,
		cfg:    cfg,
	}
}
